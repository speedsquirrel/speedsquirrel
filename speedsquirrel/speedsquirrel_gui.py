"""
(c) 2021 Nic Olsen
See LICENSE for full details
"""

from .speedsquirrel import SpeedSquirrel

import sys
import pandas as pd
import numpy as np
import os.path
import importlib.resources
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtCore import Qt

with importlib.resources.path("speedsquirrel", "logo.png") as logo:
    DEFAULT_LOGO = str(logo)

with importlib.resources.path("speedsquirrel", "logo.svg") as logo:
    SS_LOGO = str(logo)

class SpeedSquirrelGui(QMainWindow):
    def __init__(self):
        super().__init__()

        self.main = QWidget()
        self.setCentralWidget(self.main)
        self.setWindowTitle("SpeedSquirrel")
        self.setWindowIcon(QtGui.QIcon(SS_LOGO))

        self.layout = QGridLayout(self.main)

        # Track the next row of widgets to create
        self.row = 0

        # Add content
        self.add_main_layout()
        self.add_compare_layout()
        self.add_process_button()

        # Finalize GUI
        self.show()

    def add_main_layout(self):
        # Input file selection
        self.lss_file_input = QLineEdit()
        lss_label = QLabel("&LSS File:")
        lss_label.setBuddy(self.lss_file_input)
        lss_button = QPushButton("&Browse")
        lss_button.clicked.connect(self.load_file)

        self.layout.addWidget(lss_label, self.row, 0, 1, 1)
        self.layout.addWidget(self.lss_file_input, self.row, 1, 1, 2)
        self.layout.addWidget(lss_button, self.row, 3, 1, 1)
        self.row = self.row + 1

        # Player name input
        self.name_input = QLineEdit()
        name_label = QLabel("Player &Name:")
        name_label.setBuddy(self.name_input)
        self.layout.addWidget(name_label, self.row, 0, 1, 1)
        self.layout.addWidget(self.name_input, self.row, 1, 1, 3)
        self.row = self.row + 1

        # Preview table of input data
        self.time_table = QTableWidget()
        self.time_table.verticalHeader().setVisible(False)
        self.layout.addWidget(self.time_table, self.row, 0, 1, 4)
        self.row = self.row + 1

        # Reference run number selection
        ref_label = QLabel("&Reference Run (run_num):")
        self.ref_input = QLineEdit()
        self.ref_input.setEnabled(False)
        ref_label.setBuddy(self.ref_input)
        self.layout.addWidget(ref_label, self.row, 0, 1, 1)
        self.layout.addWidget(self.ref_input, self.row, 1, 1, 3)
        self.row = self.row + 1

        # Export button for current data table
        self.export_table_button = QPushButton("&Export Table")
        self.export_table_button.clicked.connect(self.export_table)
        self.export_table_button.setEnabled(False)
        self.layout.addWidget(self.export_table_button, self.row, 0, 1, 4)
        self.row = self.row + 1

        # Colors selection buttons
        self.hist_color = None
        self.hist_color_button = QPushButton("Histogram Color")
        self.hist_color_button.clicked.connect(self.pick_hist_color)

        self.line_color = None
        self.line_color_button = QPushButton("Line Color")
        self.line_color_button.clicked.connect(self.pick_line_color)

        self.ref_color = None
        self.ref_color_button = QPushButton("Reference Color")
        self.ref_color_button.clicked.connect(self.pick_ref_color)

        self.layout.addWidget(self.hist_color_button, self.row, 0, 1, 1)
        self.layout.addWidget(self.line_color_button, self.row, 1, 1, 1)
        self.layout.addWidget(self.ref_color_button, self.row, 2, 1, 1)
        self.row = self.row + 1

        # Logo image selection
        self.logo_file_input = QLineEdit(DEFAULT_LOGO)
        logo_label = QLabel("&Logo Image:")
        logo_label.setBuddy(self.logo_file_input)
        logo_button = QPushButton("&Browse")
        logo_button.clicked.connect(self.load_logo)

        self.layout.addWidget(logo_label, self.row, 0, 1, 1)
        self.layout.addWidget(self.logo_file_input, self.row, 1, 1, 2)
        self.layout.addWidget(logo_button, self.row, 3, 1, 1)
        self.row = self.row + 1

    def add_compare_layout(self):
        # Top line
        frame = QFrame()
        frame.setFrameShape(QFrame.Shape.HLine)
        self.layout.addWidget(frame, self.row, 0, 1, 4)
        self.row = self.row + 1

        # Checkbox to enable comparison
        self.compare_checkbox = QCheckBox("&Head-to-Head:")
        self.compare_checkbox.clicked.connect(self.toggle_comparison)
        self.layout.addWidget(self.compare_checkbox, self.row, 0, 1, 4)
        self.row = self.row + 1

        # Name of comparison player
        self.name_B_input = QLineEdit()
        self.name_B_input.setEnabled(False)
        name_B_label = QLabel("Player &Name:")
        name_B_label.setBuddy(self.name_B_input)

        self.layout.addWidget(name_B_label, self.row, 0, 1, 1)
        self.layout.addWidget(self.name_B_input, self.row, 1, 1, 3)
        self.row = self.row + 1

        # Data for comparison player
        self.lss_B_file_input = QLineEdit()
        self.lss_B_file_input.setEnabled(False)
        lss_B_label = QLabel("LSS File:")
        lss_B_label.setBuddy(self.lss_file_input)
        self.lss_B_button = QPushButton("&Browse")
        self.lss_B_button.setEnabled(False)
        self.lss_B_button.clicked.connect(self.load_file_B)

        self.layout.addWidget(lss_B_label, self.row, 0, 1, 1)
        self.layout.addWidget(self.lss_B_file_input, self.row, 1, 1, 2)
        self.layout.addWidget(self.lss_B_button, self.row, 3, 1, 1)
        self.row = self.row + 1

        # Bottom line
        frame = QFrame()
        frame.setFrameShape(QFrame.Shape.HLine)
        self.layout.addWidget(frame, self.row, 0, 1, 4)
        self.row = self.row + 1

    def add_process_button(self):
        # Process button
        self.process_button = QPushButton("&Process")
        self.process_button.clicked.connect(self.run_process)
        self.process_button.setEnabled(False)
        self.layout.addWidget(self.process_button, self.row, 0, 1, 3)

        self.format_picker = QComboBox()
        self.format_picker.addItems(SpeedSquirrel.image_formats)
        self.layout.addWidget(self.format_picker, self.row, 3, 1, 1)
        self.row = self.row + 1

    def toggle_comparison(self):
        compare = self.compare_checkbox.checkState() == Qt.CheckState.Checked
        self.name_B_input.setEnabled(compare)
        self.lss_B_file_input.setEnabled(compare)
        self.lss_B_button.setEnabled(compare)

    def _warn(self, msg):
        win = QMessageBox()
        win.setWindowTitle("Warning")
        win.setText(msg)
        win.setIcon(QMessageBox.Critical)
        win.exec_()

    def _notice(self, msg):
        win = QMessageBox()
        win.setWindowTitle("Notice")
        win.setText(msg)
        win.setIcon(QMessageBox.Information)
        win.exec_()

    def pick_hist_color(self):
        self.hist_color = QColorDialog.getColor().name()
        self.hist_color_button.setStyleSheet(f"background-color: {self.hist_color}")

    def pick_line_color(self):
        self.line_color = QColorDialog.getColor().name()
        self.line_color_button.setStyleSheet(f"background-color: {self.line_color}")

    def pick_ref_color(self):
        self.ref_color = QColorDialog.getColor().name()
        self.ref_color_button.setStyleSheet(f"background-color: {self.ref_color}")

    def table_to_df(self) -> pd.DataFrame:
        # Read table from the gui
        n_rows = self.time_table.rowCount()
        n_cols = self.time_table.columnCount()

        data = np.zeros((n_rows, n_cols))
        for i in range(n_rows):
            for j in range(n_cols):
                time = float(self.time_table.item(i, j).text())
                if time < 0:
                    raise ValueError("Times must be non-negative")

                data[i, j] = time

        header = [self.time_table.horizontalHeaderItem(i).text() for i in range(n_cols)]
        df = pd.DataFrame(data, columns=header)
        df.set_index("run_num", inplace=True)
        df.index = df.index.astype(int)

        return df

    def load_logo(self):
        filename = QFileDialog.getOpenFileName()[0]
        if filename != "":
            self.logo_file_input.setText(filename)

    def load_file(self):
        filename = QFileDialog.getOpenFileName(filter="All Time Files (*.lss *.csv);; LSS (*.lss);; CSV (*.csv)")[0]
        if filename != "":
            self.lss_file_input.setText(filename)

        try:
            plotter = SpeedSquirrel(lss_files = [filename])
            df = plotter.df
        except FileNotFoundError as e:
            if e.filename != "":
                self._warn(f"File \"{e.filename}\" not found")
            return

        df.insert(0, "run_num", df.index)
        n_rows, n_cols = df.shape
        self.time_table.setRowCount(n_rows)
        self.time_table.setColumnCount(n_cols)
        for row in range(n_rows):
            for col in range(n_cols):
                self.time_table.setItem(row, col, QTableWidgetItem(str(df.iloc[row, col])))

        self.time_table.setHorizontalHeaderLabels(df.columns)
        self.time_table.resizeColumnsToContents()
        self.time_table.resizeRowsToContents()

        self.export_table_button.setEnabled(True)
        self.process_button.setEnabled(True)
        self.ref_input.setEnabled(True)

    def load_file_B(self):
        filename = QFileDialog.getOpenFileName(filter="All Time Files (*.lss *.csv);; LSS (*.lss);; CSV (*.csv)")[0]
        if filename != "":
            self.lss_B_file_input.setText(filename)

    def export_table(self):
        filename = QFileDialog.getSaveFileName(filter="CSV (*.csv)")[0]
        if filename == "":
            return

        try:
            df = self.table_to_df()
        except ValueError as e:
            self._warn(str(e))
            return

        df.to_csv(filename)

    def run_process(self):
        df = self.table_to_df()

        ref = self.ref_input.text()
        if ref == "":
            reference_run = None
        else:
            try:
                reference_run = df.loc[int(ref)]
            except KeyError as e:
                self._warn(f"Invalid reference run number \"{e.args[0]}\"")
                return
            except ValueError as e:
                self._warn(f"Reference run must be a number from \"run_num\" column")
                return

        outdir = QFileDialog.getExistingDirectory()
        if outdir == "":
            return

        logo_image = self.logo_file_input.text()
        if logo_image == "":
            logo_image = None

        if self.compare_checkbox.checkState() == Qt.CheckState.Checked:
            name = self.name_input.text()
            name_B = self.name_B_input.text()
            file_B = self.lss_B_file_input.text()
            if file_B == "":
                file_B = None
            else:
                file_B = [file_B]
        else:
            name = name_B = file_B = None

        plotter = SpeedSquirrel(df=df, reference_run=reference_run,
                                outdir=outdir, hist_color=self.hist_color,
                                line_color=self.line_color,
                                reference_color=self.ref_color,
                                logo_file=logo_image,
                                name=name,
                                name_B=name_B,
                                lss_files_B=file_B,
                                image_format=self.format_picker.currentText())
        plotter.process_data()

        plot_files = plotter.saved_images
        self._notice("Generated:\n- " + "\n- ".join(plot_files))

def launch_gui():
    qapp = QApplication(sys.argv)
    app = SpeedSquirrelGui()
    qapp.exec_()

if __name__ == "__main__":
    launch_gui()
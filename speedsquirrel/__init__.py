from .speedsquirrel import SpeedSquirrel, main

try:
    import PytQt5
    from .speedsquirrel_gui import launch_gui
except ImportError:
    pass

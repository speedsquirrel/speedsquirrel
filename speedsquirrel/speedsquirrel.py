"""
(c) 2021 Nic Olsen
See LICENSE for full details
"""

from .utils import log_savefig, to_hms, style_time_plot

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patheffects as pe
import pandas as pd
from scipy import stats
from typing import List, Union
from xml.etree import ElementTree
import argparse


class MissingDataError(Exception):
    pass


class InvalidSplitData(Exception):
    pass


class SpeedSquirrel:
    image_formats = [".png", ".svg", ".jpg", ".pdf"]

    def __init__(self, lss_files: List[str] = None,
                 df: pd.DataFrame = None,
                 trust_segments: bool = False,
                 outdir: str = "./",
                 reference_run: Union[int, pd.Series] = None,
                 hist_color: str = None,
                 line_color: str = None,
                 reference_color: str = None,
                 name: str = None,
                 logo_file: str = None,
                 lss_files_B: List[str] = None,
                 df_B: pd.DataFrame = None,
                 name_B: str = None,
                 image_format: str = ".png"):
        """Organize data needed for split analysis

        Args:
            lss_files (List[str], optional): List of LiveSplit LSS files with time data. Defaults to None.
            df (pd.DataFrame, optional): DataFrame of timing data. Cannot be provided if lss_files is not None. Defaults to None.
            trust_segments (bool, optional): Trust that the order of segments matches between files? Defaults to False.
            outdir (str, optional): Output directory. Defaults to "./".
            reference_run (Union[int, pd.Series], optional): run_num to use as plot over distribution plots. Defaults to None.
            hist_color (str, optional): Color for histograms. Defaults to None.
            line_color (str, optional): Color for lines. Defaults to None.
            reference_color (str, optional): Color for the reference run. Defaults to None.
            name (str, optional): Name of the player. Defaults to None.
            logo_file (str, optional): Image to place on all plots. Defaults to None.
            lss_files_B (List[str], optional): Files for the player to be compared against. Defaults to None.
            df_B (pd.DataFrame, optional): DataFrame of times for the player to be compared againsts.
                Cannot be provided if lss_files_B is not None. Defaults to None.
            name_B (str, optional): Name of the player being compared against. Defaults to None.
            image_format (str, optional): Image suffix for output. Defaults to ".png".
        """

        self._outdir = outdir
        self._lss_files = lss_files
        self._trust_segments = trust_segments
        if ((lss_files is not None and df is not None)
                or (lss_files is None and df is None)):
            raise Exception("Either LSS files or an existing DataFrame must be given")

        if df is None:
            self._df = self.read_lss(self._lss_files, self._trust_segments)
        else:
            self._df = df.copy()

            # Add run_num if missing
            if self._df.index.name != "run_num":
                if "run_num" not in self._df.columns:
                    self._df["run_num"] = np.arange(self._df.shape[0])

                self._df.reset_index(inplace=True)
                self._df.set_index("run_num", inplace=True)

        if isinstance(reference_run, pd.Series):
            self._reference_run = reference_run.copy()
        elif isinstance(reference_run, int):
            self._reference_run = self._df.loc[reference_run]
        else:
            self._reference_run = None

        self._hist_color = hist_color
        self._line_color = line_color
        self._reference_color = reference_color

        if name is not None:
            self._name = name
        else:
            self._name = ""

        if logo_file is not None:
            self._logo_image = plt.imread(logo_file)
        else:
            self._logo_image = None

        if lss_files_B is not None or df_B is not None:
            self._name_B = name_B
            if self._name is None or self._name_B is None:
                raise MissingDataError("Both player names must be provided for comparison")

            self._squirrel_B = SpeedSquirrel(lss_files_B, df_B, trust_segments=trust_segments)
        else:
            self._squirrel_B = None

        if image_format not in self.image_formats:
            raise Exception(f"image_format must be one of {self.image_formats}")

        self.image_format = image_format

        # Images that this writer has created
        self._saved_images = []

    @property
    def df(self):
        return self._df.copy()

    @df.setter
    def df(self, df):
        self._df = df.copy()

    @property
    def saved_images(self):
        return self._saved_images.copy()

    def process_data(self):
        """Produce all plots for the split data"""
        self._df.sort_values("run_num", inplace=True)

        # Per-segment distributions
        segments = self._df.columns
        for seg in segments:
            self.plot_segment(seg)

        # Cumulative runs
        self.plot_cumulative()

        # Non-cumulative runs
        self.plot_noncumulative()

        # Run total distributions sampling from incomplete data
        self.plot_sampled_distribution()

        # Run total distributions from completed runs
        self.plot_total_distribution()

        if self._squirrel_B is not None:
            self.plot_head_to_head()

    @staticmethod
    def read_lss(lss_files: List[str], trust_segments: bool = False) -> pd.DataFrame:
        """Convert LiveSplit LSS files into a DataFrame of all run segmented times

        Args:
            lss_files (List[str]): List of LSS files output by LiveSplit (Segments _must_ match)
            trust_segments (bool, optional): Do not force segment names to match between LSS files and
                instead trust that they represent the same segments in the same order? Defaults to False.

        Returns:
            pd.DataFrame: Collection of segment times for each run
        """

        file_dfs = []
        for lss_file in lss_files:
            if lss_file.lower().endswith(".csv"):
                df = pd.read_csv(lss_file, index_col=None)
                if "run_num" in df.columns:
                    df.drop("run_num", axis=1, inplace=True)

                file_dfs.append(df)
                continue

            run = ElementTree.parse(lss_file).getroot()
            # Record all duration entries for each segment labeled by their ids
            seg_map = {}
            for seg in run.findall(".//Segment"):
                name = seg.find("Name").text
                seg_map[name] = {}
                for time in seg.findall(".//SegmentHistory/Time"):
                    seg_map[name][time.attrib["id"]] = pd.to_timedelta(
                        time.find("RealTime").text).seconds

            # Create a row for each unique id
            unique_ids = list(set.union(*[set(seg.keys())
                                          for seg in seg_map.values()]))
            rows = [[seg_map[seg][m_id] if m_id in seg_map[seg]
                    else None for seg in seg_map] for m_id in sorted(unique_ids)]

            df_file = pd.DataFrame(rows, columns=seg_map.keys(), dtype=float)
            df_file.index.rename("run_num", inplace=True)

            file_dfs.append(df_file)

        if not trust_segments:
            # Columns _must_ match
            # ie Each LSS file provided must have exactly the same segment names
            if not all([(dfi.columns == dfj.columns).all()
                        for i, dfi in enumerate(file_dfs)
                        for _, dfj in enumerate(file_dfs[i:], i)]):
                raise InvalidSplitData(
                    "All LSS files must have identical split names. If you are certain that segments correspond between files, specify ")

        # Combine all file contents into one set of runs
        df = pd.concat(file_dfs)
        df.reset_index(inplace=True, drop=True)
        df.index.rename("run_num", inplace=True)

        return df

    def plot_segment(self, seg: str, outfile: str = None):
        """Plot time distribution for the given segment

        Args:
            seg (str): Segment label to process
            outfile (str, optional): Name of output plot file. Defaults to None.
        """

        if outfile is None:
            outfile = f"segment_{seg.replace(' ', '_')}.svg"

        times = self._df[seg].dropna()
        mean = np.mean(times)
        std = np.std(times)

        plt.figure()
        plt.title(
            f"{self._name}\n{seg.title()}\n"
            r"$\mu$"
            f"= {to_hms(mean)}; "
            r"$\sigma$"
            f" = {to_hms(std)}")

        ax_hist = plt.gca()
        ax_dist = plt.gca().twinx()

        ax_hist.set_ylabel("Count")
        ax_hist.hist(times, color=self._hist_color)
        x = np.linspace(np.min(times), np.max(times))

        ax_dist.set_ylabel("$p$")
        ax_dist.plot(x, stats.norm.pdf(x, mean, std),
                     "--", color=self._line_color)

        if self._reference_run is not None:
            plt.axvline(self._reference_run[seg], color=self._reference_color,
                        path_effects=[
                            pe.Stroke(linewidth=3, foreground="k"), pe.Normal()],
                        label=f"Run {self._reference_run.name}")
            plt.legend()

        style_time_plot(x_axis_time=True, ax=ax_hist)
        self.plot_images()
        self._saved_images.append(outfile)
        log_savefig(outfile, self._outdir, override_suffix=self.image_format)
        plt.close()

    def plot_cumulative(self, outfile: str = "cumulative.svg"):
        """Plot all run cumulative times

        Args:
            outfile (str, optional): Name of output plot file. Defaults to "cumulative.svg".
        """

        plt.figure()
        for run_num, times in self._df.dropna().iterrows():
            scan = np.cumsum(times)
            plt.plot(scan)

        bests = self._df.min(axis=0)
        plt.plot(np.cumsum(bests), "--", label="Bests", color=self._line_color)
        sum_of_bests = bests.sum()
        plt.title(
            f"{self._name}\nRuns Against All-Time Bests\nSum of Bests: {to_hms(sum_of_bests)}")
        plt.xticks(rotation=45)

        style_time_plot(y_axis_time=True)
        self.plot_images()
        self._saved_images.append(outfile)
        log_savefig(outfile, self._outdir, override_suffix=self.image_format)
        plt.close()

    def plot_noncumulative(self, outfile: str = "all_segments.svg"):
        """Plot whole runs, but with the segment times non-cumulative

        Args:
            outfile (str, optional): Name of output plot file. Defaults to "all_segments.svg".
        """

        bests = self._df.min(axis=0)

        plt.figure()
        plt.title(f"{self._name}\nSeparated Segments")
        plt.xticks(rotation=45)
        for run_num, times in self._df.iterrows():
            plt.plot(times, "--*", alpha=0.2)

        plt.plot(bests, "--", label="Bests", color=self._line_color)
        plt.legend()

        style_time_plot(y_axis_time=True)
        self.plot_images()
        self._saved_images.append(outfile)
        log_savefig(outfile, self._outdir, override_suffix=self.image_format)
        plt.close()

    def plot_sampled_distribution(self, n_samples: int = 10000,
                                  outfile_pdf: str = "sampled_total_pdf.svg",
                                  outfile_cdf: str = "sampled_total_cdf.svg"):
        """Plot distribution of full-run times computed by sampling each segment independently and summing.

        Args:
            n_samples (int, optional): Number of samples to draw from each segment. Defaults to 10000.
            outfile_pdf (str, optional): Name of output plot pdf file. Defaults to "sampled_total_pdf.svg".
            outfile_cdf (str, optional): Name of output plot cdf file. Defaults to "sampled_total_cdf.svg".
        """

        # Sample each segment distribution because there might be missing entries in df
        sums = self.sample_totals(n_samples=n_samples)

        mean = np.mean(sums)
        std = np.std(sums)

        # PDF
        plt.figure()
        plt.title(
            f"{self._name}\nPDF Totals from {n_samples} samples\n"
            r"$\mu$"
            f" = {to_hms(mean)}; "
            r"$\sigma$"
            f" = {to_hms(std)}")
        plt.ylabel("$p$")

        plt.hist(sums, density=True, color=self._hist_color)
        x = np.linspace(sums.min(), sums.max())
        plt.plot(x, stats.norm.pdf(x, mean, std), "--", color=self._line_color)
        if self._reference_run is not None:
            plt.axvline(self._reference_run.sum(), color=self._reference_color,
                        path_effects=[
                            pe.Stroke(linewidth=3, foreground="k"), pe.Normal()],
                        label=f"Run {self._reference_run.name}")
            plt.legend()

        plt.grid()
        style_time_plot(x_axis_time=True)
        self.plot_images()
        self._saved_images.append(outfile_pdf)
        log_savefig(outfile_pdf, self._outdir, override_suffix=self.image_format)
        plt.close()

        # CDF
        plt.figure()
        plt.title(
            f"{self._name}\nCDF Totals from {n_samples} samples\n"
            r"$\mu$"
            f" = {to_hms(mean)}; "
            r"$\sigma$"
            f" = {to_hms(std)}")
        plt.ylabel("$p$")

        plt.plot(x, stats.norm.cdf(x, mean, std), "--", color=self._line_color)
        if self._reference_run is not None:
            plt.axvline(self._reference_run.sum(), color=self._reference_color,
                        path_effects=[
                            pe.Stroke(linewidth=3, foreground="k"), pe.Normal()],
                        label=f"Run {self._reference_run.name}")
            plt.legend()

        plt.grid()
        plt.minorticks_on()
        plt.grid(True, which="minor", axis="y", linestyle="--")
        style_time_plot(x_axis_time=True)
        self.plot_images()
        self._saved_images.append(outfile_cdf)
        log_savefig(outfile_cdf, self._outdir, override_suffix=self.image_format)
        plt.close()

    def sample_totals(self, n_samples: int = 1000):
        """Sample each segment independently to produce full-run times

        Args:
            n_samples (int, optional): Number of runs to sample. Defaults to 1000.

        Returns:
            np.array: Sampled full-run times of length n_samples
        """
        sums = np.zeros(n_samples)
        for seg in self._df.columns:
            times = self._df[seg].dropna()
            sums = sums + np.random.choice(times, n_samples)

        return sums

    def plot_total_distribution(self, outfile_pdf: str = "total_pdf.svg",
                                outfile_cdf: str = "total_cdf.svg"):
        """Plot distribution of full-run times from completed runs only.

        Args:
            outfile_pdf (str, optional): Name of output pdf plot file. Defaults to "total_pdf.svg".
            outfile_cdf (str, optional): Name of output cdf plot file. Defaults to "total_cdf.svg".
        """

        sums = self._df.dropna().sum(axis=1)
        mean = np.mean(sums)
        std = np.std(sums)

        # PDF
        plt.figure()
        plt.title(
            f"{self._name}\nPDF Totals from Completed Runs\n"
            r"$\mu$"
            f" = {to_hms(mean)}; "
            r"$\sigma$"
            f" = {to_hms(std)}")
        plt.ylabel("$p$")

        plt.hist(sums, density=True, color=self._hist_color)
        x = np.linspace(sums.min(), sums.max())
        plt.plot(x, stats.norm.pdf(x, mean, std), "--", color=self._line_color)
        if self._reference_run is not None:
            plt.axvline(self._reference_run.sum(), color=self._reference_color,
                        path_effects=[
                            pe.Stroke(linewidth=3, foreground="k"), pe.Normal()],
                        label=f"Run {self._reference_run.name}")
            plt.legend()

        style_time_plot(x_axis_time=True)
        plt.grid()
        self.plot_images()
        self._saved_images.append(outfile_pdf)
        log_savefig(outfile_pdf, self._outdir, override_suffix=self.image_format)
        plt.close()

        # CDF
        plt.figure()
        plt.title(
            f"{self._name}\nCDF Totals from Complete Runs\n"
            r"$\mu$"
            f" = {to_hms(mean)}; "
            r"$\sigma$"
            f" = {to_hms(std)}")
        plt.ylabel("$p$")

        plt.plot(x, stats.norm.cdf(x, mean, std), "--", color=self._line_color)
        if self._reference_run is not None:
            plt.axvline(self._reference_run.sum(), color=self._reference_color,
                        path_effects=[
                            pe.Stroke(linewidth=3, foreground="k"), pe.Normal()],
                        label=f"Run {self._reference_run.name}")
            plt.legend()

        plt.grid()
        plt.minorticks_on()
        plt.grid(True, which="minor", axis="y", linestyle="--")
        style_time_plot(x_axis_time=True)
        self.plot_images()
        self._saved_images.append(outfile_cdf)
        log_savefig(outfile_cdf, self._outdir, override_suffix=self.image_format)
        plt.close()

    def plot_head_to_head(self, outfile_pdf: str = "head_to_head_pdf.svg",
                          outfile_cdf: str = "head_to_head_cdf.svg"):
        """Plot comparison of two players including predictions for full-run results.

        Args:
            outfile_pdf (str, optional): Name of output pdf plot file. Defaults to "head_to_head_pdf.svg".
            outfile_cdf (str, optional): Name of output cdf plot file. Defaults to "head_to_head_cdf.svg".
        """

        sums_A = self._df.dropna().sum(axis=1)
        mean_A = np.mean(sums_A)
        std_A = np.std(sums_A)

        sums_B = self._squirrel_B.df.dropna().sum(axis=1)
        mean_B = np.mean(sums_B)
        std_B = np.std(sums_B)

        mean_diff = mean_A - mean_B
        std_diff = np.sqrt(std_A**2 + std_B**2)

        # PDF
        plt.figure()
        if mean_diff < 0:
            title = f"{self._name_B} is expected to win by {-mean_diff:0.1f} seconds"
        else:
            title = f"{self._name} is expected to win by {mean_diff:0.1f} seconds"

        plt.title(
            f"{title}\nDifferential Completed Runs PDF "
            r"$\mu$"
            f" = {to_hms(mean_diff)}; "
            r"$\sigma$"
            f" = {to_hms(std_diff)}")
        plt.ylabel("$p$")
        plt.xlabel(f"Differential ({self._name} - {self._name_B}) [sec]")

        x = np.linspace(mean_diff - 3 * std_diff, mean_diff + 3 * std_diff)
        plt.plot(x, stats.norm.pdf(x, mean_diff, std_diff),
                 "--", color=self._line_color)

        plt.grid()
        self.plot_images()
        self._saved_images.append(outfile_pdf)
        log_savefig(outfile_pdf, self._outdir, override_suffix=self.image_format)
        plt.close()

        # CDF
        plt.figure()
        p_A_wins = stats.norm.cdf(0, mean_diff, std_diff)
        if mean_diff < 0:
            title = f"{self._name_B} is expected to win with probability {1 - p_A_wins:0.1f}"
        else:
            title = f"{self._name} is expected to win with probability {p_A_wins:0.1f}"

        plt.title(
            f"{title}\nDifferential Complete Runs CDF "
            r"$\mu$"
            f" = {to_hms(mean_diff)}; "
            r"$\sigma$"
            f" = {to_hms(std_diff)}")
        plt.ylabel("$p$")
        plt.xlabel(f"Differential ({self._name} - {self._name_B}) [sec]")

        plt.plot(x, stats.norm.cdf(x, mean_diff, std_diff),
                 "--", color=self._line_color)
        plt.plot(0, p_A_wins, "*", color=self._reference_color)

        plt.grid()
        plt.minorticks_on()
        plt.grid(True, which="minor", axis="y", linestyle="--")

        self.plot_images()
        self._saved_images.append(outfile_cdf)
        log_savefig(outfile_cdf, self._outdir, override_suffix=self.image_format)
        plt.close()

    def plot_images(self, fig: plt.Figure = None):
        if fig is None:
            fig = plt.gcf()

        if self._logo_image is not None:
            ax = fig.add_axes((0.8, 0.8, 0.2, 0.2), zorder=1, anchor="NE")
            ax.imshow(self._logo_image)
            ax.axis("off")


def main():
    parse = argparse.ArgumentParser(description="Plot summaries of speedrunning data")
    parse.add_argument("data_files", nargs="+",
                       help="LiveSplit LSS or CSV files containing segment times to analyze")
    parse.add_argument("--outdir", help="Output directory", default="./")
    parse.add_argument("--hist_color", help="Color to use for histogram plots")
    parse.add_argument("--line_color", help="Color to use for line plots")
    parse.add_argument("--ref_color", help="Color to use for the reference run overlays")
    parse.add_argument("--logo_image", help="Image to overlay on plots")
    parse.add_argument("--name", help="Name of player")
    parse.add_argument("--name_B", help="Name of player being compared against")
    parse.add_argument("--data_files_B", nargs="+",
                       help="LiveSplit LSS or CSV files containing segment times for player B")
    parse.add_argument("--image_format", help=f"Image format from {SpeedSquirrel.image_formats}")
    parse.add_argument("--trust_segments", action="store_true",
                       help="Trust that segments are in the same order and correspond between LSS files")
    args = parse.parse_args()

    plotter = SpeedSquirrel(args.data_files, outdir=args.outdir,
                            hist_color=args.hist_color, line_color=args.line_color,
                            reference_color=args.ref_color, logo_file=args.logo_image,
                            lss_files_B=args.data_files_B, name=args.name, name_B=args.name_B,
                            image_format=args.image_format, trust_segments=args.trust_segments)
    plotter.process_data()


if __name__ == "__main__":
    main()

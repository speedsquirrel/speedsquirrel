"""
(c) 2021 Nic Olsen
See LICENSE for full details
"""

import numpy as np
import pandas as pd
import argparse
from string import ascii_lowercase

def gen_data(data_file, labels, means, stds, n=1000):
    df = pd.DataFrame({l: np.maximum(0, np.random.normal(m, s, n)) for l, m, s in zip(labels, means, stds)})
    df.index.rename("run_num", inplace=True)
    df.to_csv(data_file)

def main():
    parse = argparse.ArgumentParser()
    parse.add_argument("data_file")
    parse.add_argument("--n_splits", type=int, default=15)
    args = parse.parse_args()

    n_splits = args.n_splits

    labels = [f"split_{c}" for c in ascii_lowercase[:n_splits]]
    means = 60 * 5 * np.random.rand(n_splits)
    stds = 60 * np.random.rand(n_splits)

    gen_data(args.data_file, labels, means, stds)

if __name__ == "__main__":
    main()
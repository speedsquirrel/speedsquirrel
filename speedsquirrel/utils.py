import logging
from datetime import timedelta
import matplotlib.pyplot as plt
import os.path

logging.root.setLevel(logging.INFO)

def log_savefig(filename: str, outdir: str = "./", override_suffix: str = None):
    """Wraps plt.savefig with a logger message

    Args:
        filename (str): Filename for the figure
        outdir (str, optional): Output directory. Defaults to "./".
        override_suffix (str, optional): Override the given file suffix ("." and after) with this. Defaults to None.
    """

    if override_suffix is not None and "." in filename:
        end = filename.rindex(".")
        filename = f"{filename[:end]}{override_suffix}"

    outfilename = os.path.join(outdir, filename)

    plt.savefig(outfilename, bbox_inches="tight")
    logging.info(f"Generated {outfilename}")


def to_hms(sec: float) -> str:
    """Utility function for converting time in seconds to
    a formated string:

    HH:MM:SS if more than 1 hour; MM:SS if less than 1 hour

    Args:
        sec (float): Duration in seconds

    Returns:
        str: Formated time string
    """

    s = str(timedelta(seconds=sec))

    # Remove fractional seconds
    if "." in s:
        s = "".join(s.split(".")[:-1])

    # Remove leading 0 hours
    if s.startswith("0:"):
        s = s[2:]

    return s


def style_time_plot(x_axis_time: bool = False, y_axis_time: bool = False,
                    ax: plt.Axes = None):
    """Apply time formatting to the given axes

    Args:
        x_axis_time (bool, optional): Apply to X-axis?. Defaults to False.
        y_axis_time (bool, optional): Apply to Y-axis. Defaults to False.
        ax (plt.Axes, optional): Axes to format. Defaults to None, which takes plt.gca().
    """
    if ax is None:
        ax = plt.gca()

    ax.set_ylim(bottom=0)

    if x_axis_time:
        ticks_sec = ax.get_xticks()
        ticks_hms = [to_hms(s) for s in ticks_sec]
        ax.set_xticks(ticks_sec, ticks_hms, rotation=45)
        ax.set_xlabel("Time [(hh:)mm:ss]")
        left, _ = ax.get_xlim()
        ax.set_xlim(left=max(left, 0))

    if y_axis_time:
        ticks_sec = ax.get_yticks()
        ticks_hms = [to_hms(s) for s in ticks_sec]
        ax.set_yticks(ticks_sec, ticks_hms)
        ax.set_ylabel("Time [(hh:)mm:ss]")
        bottom, _ = ax.get_ylim()
        ax.set_ylim(bottom=max(bottom, 0))
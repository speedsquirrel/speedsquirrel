"""
(c) 2021 Nic Olsen
See LICENSE for full details
"""

import speedsquirrel as ss
import matplotlib.pyplot as plt
import numpy as np
import os


def test_log_savefig():
    filename = "tmp.png"
    t = np.linspace(0, 2 * np.pi)
    plt.figure()
    plt.plot(t, np.sin(t))
    ss.utils.log_savefig(filename)
    assert os.path.exists(filename)


def test_to_hms():
    sec = [90, 39, 60 * 60 + 34 * 60 + 7, 2.23]
    hms = ["01:30", "00:39", "1:34:07", "00:02"]
    for s, h in zip(sec, hms):
        assert ss.utils.to_hms(s) == h


def test_read_lss():
    pass

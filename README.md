# Speed Squirrel

<div align="center"><img src="speedsquirrel/logo.svg" width="15%" alt="Speed Squirrel logo"></div>

Data analytics for speedrunners. Turn [LiveSplit](https://livesplit.org/) or spreadsheet split times into useful speedrunning statistics for analyzing runs, tracking stats, and predicting race outcomes with intuitive graphical tools.

_Author_: Nic Olsen  
_Logo Design_: Mady Veith


## Features

- Insightful speedrunning performance statistics:
    - Per-split distributions
    - Cumulative run timelines
    - Complete and sampled-incomplete full-run time distributions
        - Performance probability predictions
    - Specific reference run comparison against distributions
- Head-to-head race predictions:
    - Expected winning margin
    - Probability of winning
- Accepts common LiveSplit `.lss` files and `.csv` spreadsheets
- Customizable color schemes
- Customizable overlay images
- `png`, `svg`, `pdf`, and `jpg` output formats
- Intuitive graphical user interface
    - Table editor for viewing, editing, and exporting data before processing
- Command line interface for shell-based automation pipelines (particularly for live website updates)
- Importable as a Python module for Python-based automation pipelines (particularly for Discord bots)
- Platform independent
- Open source


## \* Beta Notes \*

Thanks for taking part in the Speed Squirrel beta!

Please provide feedback with a [GitLab issue](https://gitlab.com/speedsquirrel/speedsquirrel/-/issues) including:

- A description of your issue, input, or feature request
    - Please only include one topic per issue.
    - Please search for related issues before posting.
- Your operating system and Python version


_Community Questions:_

- Were there complications or instructions missing in the install process?
- Are there other split data formats that you use?
- What inconveniences did you have working with the tools?
- What parts of the interface do you think could be more intuitive?
- Are there any other statistics you would like supported?


## Installation

Requires:

- Python 3.8 or greater
- Git
- GL

From a shell, run the following. Add `--user` after `install` if you do not have administrative privileges or do not wish to install system-wide. After beta, the package will be deployed to common package indexes.

```
pip install git+https://gitlab.com/speedsquirrel/speedsquirrel.git
```

\* On Windows, this will likely give a warning near the end of the output that the installed scripts are not on your Path. If so, add an entry to your system environment variable called `Path` with the contents being the path from the warning. This will make the scripts in [Usage](#usage) available via the Start menu or the command line.


## Usage

- `speedsquirrel`: GUI for selecting LSS files and launching analytics
- `speedsquirrel_cli`: Analytics without the GUI for automation and/or headless deployment


## License

See [LICENSE](/LICENSE)


## Contributing

See [CONTRIBUTING](/CONTRIBUTING)